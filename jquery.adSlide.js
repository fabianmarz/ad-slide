/*=============================================
    
    AdSlide Plugin
    author: Fabian Marz <fm@millenium.de>
    Version 0.1
    License: DBAD - http://www.dbad-license.org/

=============================================*/

(function($){
    $.fn.adSlide = function(options){
        if($(this.selector).length > 0){
            // get settings & set defaults
            var settings  = $.extend({
                image: null,
                width: null,
                height: null,
                link: null,
                blank: false,
                position: 'fixed',
                cssClose: 'close'
            }, options );

            if(settings.width == null){
                console.log('You have to define the width!');
                return;
            }
            
            $(this.selector).append('<div class="'+settings.cssClose+'"></div>');
            // define variables
            var image       = settings.image,
                element     = this.selector,
                close       = $(element+ ' .'+settings.cssClose),
                closeWidth  = close.outerWidth(),
                elHeight    = settings.height,
                elWidth     =  settings.width,
                regex       = /(\d+)/g,
                blank       = settings.blank,
                closeMargin = close.css('margin-left').match(regex),
                pureWidth   = elWidth.match(regex),
                finalWidth  = pureWidth - (closeWidth + parseInt(closeMargin[0])),
                link = settings.link;

            // append header style
            $('head').append('<style type="text/css"> '+element+'{position: '+ settings.position +'; background: url('+ image +'); height: '+ elHeight+'; width: '+ elWidth +'; right: 0; } '+element+' .close{ cursor: pointer; } '+element+' a{ width: '+finalWidth+'px; display: inline-block; position: absolute; right: 0; bottom:0; height: '+elHeight+' }</style>');
            $(element).append('<a href="'+link+'" '+ (blank == true ? ' target="_blank"' : '')+'></a>');

            close.bind('click', function(){
                var elMargin  = $(element).css('marginRight'),
                    newMargin = elMargin == '0px' ? '-' + finalWidth + 'px' : 0;

                $(element).stop(true, true).animate({
                    marginRight: newMargin
                }, 1000, 'easeOutExpo');
                
            });
        }
    }
}(jQuery));
